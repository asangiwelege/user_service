package com.userservice.UserService.repositories;
import com.userservice.UserService.entities.entity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
interface UserRepository extends JpaRepository<entity,Long>{
    @Query("SELECT ue FROM entity ue WHERE ue.id=?1")
    entity findUserEntityById(Long id);
}

public class repository {



}
