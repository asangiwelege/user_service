package com.userservice.UserService.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="user")

public class entity {
    @Id
    public Long Id;
    private String name;
    private String age;

    public Long getId() {return Id;}
    public void setId(Long Id) {this.Id=Id;}

    public String getName() {return name;}
    public void setName(String name) {this.name=name;}

    public String getAge() {return age;}
    public void setAge(String age) {this.age=age;}
}
