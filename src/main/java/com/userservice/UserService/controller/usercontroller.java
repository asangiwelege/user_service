package com.userservice.UserService.controller;

import com.userservice.UserService.DTO.userDTO;
import com.userservice.UserService.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;


@RestController
@RequestMapping("api/user")

public class usercontroller{
    @Autowired
    private UserService userService;

    @GetMapping("/getAll")
    public List<userDTO> getAllUsers(){
        /**previous code,
         * return Arrays.asList(
                new userDTO("1", "Thushan", "24"),
                new userDTO("2","nishani","22")
        );*/
        /**new code*/
        return UserService.getAllUsers();

    }


}
