package com.userservice.UserService.services;
import com.userservice.UserService.DTO.userDTO;
import com.userservice.UserService.entities.entity;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.repository.support.Repositories;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class UserService {
    private static final Logger LOGGER= (Logger) LoggerFactory.getLogger(UserService.class);
@Autowired
private RestTemplateBuilder restTemplate;

@Autowired
private static Repositories repositories;

    /**
     * =========================================
     * This method is used to retrieve all user data.
     * =========================================
     * @return
     */

    public static List<userDTO> getAllUsers(){
        LOGGER.info("/================ENTER INFO getUserService in Userservice==============/");
        List<userDTO> users =null;
        try{


            users = repositories.findAll()//UserEntity
                    .stream()
                    .map(entity-> new userDTO(
                            entity.getId().toString(),
                            entity.getName(),
                            entity.getAge()
                    )).collect(Collectors.toList());
        }
        catch (Exception e) {LOGGER.warning("/***************Expectation in userservice ->getAllUsers()*/"+ e);}
        return users;

    }

}

